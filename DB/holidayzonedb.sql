-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2017 at 02:56 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `holidayzonedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `DestinationId` int(11) NOT NULL,
  `DestinationName` varchar(100) NOT NULL,
  `DestinationImage` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`DestinationId`, `DestinationName`, `DestinationImage`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'Munnar', 'hotel_0111.jpg', '2017-07-31 13:39:29', '2017-07-31 13:39:29'),
(2, 'Wayanadu', 'hotel_0111.jpg', '2017-07-31 13:39:29', '2017-07-31 13:39:29'),
(3, 'outty', 'hotel_0111.jpg', '2017-07-31 13:40:30', '2017-07-31 13:40:30'),
(4, 'Palakadu', 'hotel_0111.jpg', '2017-07-31 13:40:30', '2017-07-31 13:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `PackageId` int(11) NOT NULL,
  `PackageTitle` varchar(500) NOT NULL,
  `PackageDescription` varchar(1000) NOT NULL,
  `PackageImage` varchar(100) NOT NULL,
  `PackageAmount` float NOT NULL,
  `DestinationId` int(11) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`PackageId`, `PackageTitle`, `PackageDescription`, `PackageImage`, `PackageAmount`, `DestinationId`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'KTDC Package', 'Test Description', '1.jpg', 350, 1, '2017-08-01 12:15:25', '2017-08-01 12:15:25'),
(2, 'Munnar Package', 'Munar', '2.jpg', 500, 2, '2017-08-01 12:15:25', '2017-08-01 12:15:25'),
(3, 'Cherayi Beach', 'Cerayi ', '3.jpg', 500, 3, '2017-08-01 12:15:55', '2017-08-01 12:15:55'),
(4, 'Kochi Darshan', 'Dutch Palace (Friday Holiday), Synagogue (Friday Saturday Holiday), St. Francis Church (Sunday Holiday),\r\n\r\nChinese Fishing Nets, Maritime Museum (Monday Holiday), Dutch Cemetery, Santa Cruz Catherdral.', '4.jpg', 3500, 3, '2017-08-02 06:16:43', '2017-08-02 06:16:43'),
(5, 'Sunset Cruise', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries but also the leap into electronic typesetting, remaining essentially unchanged', '2.jpg', 500, 3, '2017-08-02 06:24:25', '2017-08-02 06:24:25'),
(6, 'Kovalam Tour', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries but also the leap into electronic typesetting, remaining essentially unchanged', '1.jpg', 400, 1, '2017-08-02 06:24:25', '2017-08-02 06:24:25'),
(7, 'Ponmudi Tour', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries but also the leap into electronic typesetting, remaining essentially unchanged', '5.jpg', 2500, 4, '2017-08-02 06:24:25', '2017-08-02 06:24:25'),
(8, 'Explore Kerala', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries but also the leap into electronic typesetting, remaining essentially unchanged', '6.jpg', 2500, 1, '2017-08-02 06:24:25', '2017-08-02 06:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `package_images`
--

CREATE TABLE `package_images` (
  `ImageId` int(11) NOT NULL,
  `PackageId` int(11) NOT NULL,
  `ImageName` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`DestinationId`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`PackageId`);

--
-- Indexes for table `package_images`
--
ALTER TABLE `package_images`
  ADD PRIMARY KEY (`ImageId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `DestinationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `PackageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `package_images`
--
ALTER TABLE `package_images`
  MODIFY `ImageId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {
    

	 public function __construct() {
    //load database in autoload libraries 
      	parent::__construct(); 
		$this->load->helper('url_helper');
      	$this->load->model('DestinationModel');         
		$this->load->model('PackageModel');         
   }

   public function v1_getallpackages(){
		$package_data =$this->PackageModel->get_all_packages();
		echo json_encode($package_data);
	}

    public function v1_getalldestination(){
		$destination_data =$this->DestinationModel->get_destinations();
		echo json_encode($destination_data);
	}

}

?>
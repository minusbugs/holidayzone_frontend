<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    

	 public function __construct() {
    //load database in autoload libraries 
      	parent::__construct(); 
		$this->load->helper('url_helper');
      	$this->load->model('DestinationModel');         
		$this->load->model('PackageModel');         
   }
    public function index()
	{
		
		$data['main_title']="HolidayZone -Kerala Tourism ";
		$data['title_name']="home";
		$data['destinations_details'] = $this->DestinationModel->get_destinations();
		$data['packages_details'] =$this->PackageModel->get_all_packages();
		$this->load->view('pages/home_page',$data);
	
	}

	public function packages()
	{
		
		$data = array('content'=>'pages/packages_page',
					  'msg'=>'Pacakagesssssssssss',
					  'main_title'=>'HolidayZone -Kerala Tour Package | Package Tour Kerala | Kerala Tourism Package ',
					  'title_name'=>'packages',
					  'packages_details' =>$this->PackageModel->get_all_packages(),
					  );
		$this->load->view('includes/template',$data);
	}
    

    public function destinations()
	{
		$data = array('content'=>'pages/destinations_page',
					  'title_name'=>'destination',
					  'main_title'=>'HolidayZone -Kerala Tour Place | Destination Tour Kerala | Kerala Tourism Destination ',
					  'destinations_details'=>$this->DestinationModel->get_destinations(),
					  'packages_details' =>$this->PackageModel->get_all_packages());
		$this->load->view('includes/template',$data);
	}

    public function about()
	{
		$data = array('content'=>'pages/about_page',
					  'title_name'=>'aboutus',
					  'main_title'=>'HolidayZone -About HolidayZone',
					  'packages_details' =>$this->PackageModel->get_all_packages());
		$this->load->view('includes/template',$data);
	}

    public function reachus()
	{
		$data = array('content'=>'pages/reachus_page',
					  'title_name'=>'reachus',
					  'main_title'=>'HolidayZone -Contact HolidayZone',
					  'packages_details' =>$this->PackageModel->get_all_packages());
		$this->load->view('includes/template',$data);
	}
	public function terms()
	{
		$data = array('content'=>'pages/terms_page',
						'title_name'=>'',
						'main_title'=>'HolidayZone -About HolidayZone',
						'packages_details' =>$this->PackageModel->get_all_packages());
		$this->load->view('includes/template',$data);
	}
	public function privacy()
	{
		$data = array('content'=>'pages/privacy_page',
					  'title_name'=>'',
					  'main_title'=>'HolidayZone -About HolidayZone',
					  'packages_details' =>$this->PackageModel->get_all_packages());
		$this->load->view('includes/template',$data);
	}
	

	

	public function PackageDeatils($id){
		
	}

	public function DestinationDetails($id){
		
		$destination = $this->DestinationModel->get_destination_id($id);
		$data = array('content'=>'pages/packages_by_destination_page',
					'msg'=>'Pacakagesssssssssss',
					'title_name'=>'',
					'main_title'=>'HolidayZone -About HolidayZone',
					'packages_details' =>$this->PackageModel->get_all_packages_by_destinaionid($id),
					'destination_name'=>$destination->DestinationName,
					);
		// print_r($this->PackageModel->get_all_packages_by_destinaionid($id));exit;
		$this->load->view('includes/template',$data);
	}


	
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PackageModel extends CI_Model{

    var $table = 'packages';
    function __construct()
    {
       $this->load->database();
    }
    public function get_all_packages(){

        $this->db->select('packages.PackageId,packages.PackageTitle,packages.PackageDescription,packages.PackageImage,packages.PackageAmount,destinations.DestinationName');
        $this->db->from('packages');
        $this->db->join('destinations','packages.DestinationId=destinations.DestinationId');
        $this->db->order_by('packages.CreatedOn','desc');
        $query = $this->db->get();
        return $query->result_array();
       
    }

    public function get_all_packages_by_destinaionid($DestinationId){

        $this->db->select('packages.PackageId,packages.PackageTitle,packages.PackageDescription,packages.PackageImage,packages.PackageAmount,destinations.DestinationName');
        $this->db->from('packages');
        $this->db->join('destinations','packages.DestinationId=destinations.DestinationId');
        $this->db->where('packages.DestinationId',$DestinationId);
        $this->db->order_by('packages.CreatedOn','desc');
        $query = $this->db->get();
        return $query->result_array();
       
    }

}

?>
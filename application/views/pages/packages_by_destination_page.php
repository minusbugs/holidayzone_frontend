<section class="section lb page-title little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix">
                    <div class="pull-left">
                        <h3>All Packages Under <?php echo $destination_name;?></h3>
                        
                    </div>

                    <div class="pull-right hidden-xs">
                        <ul class="breadcrumb">
                            <li><a href="javascript:void(0)">Home</a></li>
                            <li><a href="javascript:void(0)">Packages</a></li>
                            <li class="active"><?php echo $destination_name;?> packages</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Bind Package Details   -->

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div class="blog-wrapper blog-masonry row">
                        <?php 
                             $i=0;
                                foreach ($packages_details as $packages) { 
                                    $redirect_url=base_url().'pages/PackageDeatils/'. $packages['PackageId'];
                                   
                                    $div_priority="";
                                    if($i==0){
                                        $div_priority="first";
                                    }elseif($i==2){
                                        $div_priority="last";
                                       
                                    }
                                    $package_description=$packages["PackageDescription"];
                                    $package_description = character_limiter($package_description,80);

                        ?>
                        <div class="blog-item col-md-4 <?php echo $div_priority; ?> ">
                           <a href="<?php echo  $redirect_url;  ?>" title="">
                                <div class="entry" >
                                    <img src="http://adm.holidayzone.info/assets/PackageImages/<?php echo $packages['PackageImage'];  ?>" alt="" class="img-responsive">
                                    <div class="magnifier">
                                       
                                        <div class="magni-desc">
                                        <!-- end rating -->
                                        </div>
                                    </div><!-- end magnifier -->
                                     <div class="car-price"><p>₹<?php echo $packages['PackageAmount'];  ?></p></div>
                                </div><!-- end entry -->
                            </a>
                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-map-marker"></i><?php echo $packages['DestinationName']; ?> </a></li>
                                    
                                </ul>
                                <h4><a href="<?php echo  $redirect_url;  ?>" title=""><?php echo  $packages['PackageTitle'];;  ?></a></h4>
                                <p><?php echo $package_description; ?>  </p>
                                 
                                <a href="<?php echo  $redirect_url;  ?>" class="btn btn-primary">View Deatils</a>
                            </div>
                        </div>
                    <?php
                        $i=$i+1;
                        if($i==3){
                            $i=0;
                        }
                    }
                    ?>

                    
                    </div><!-- end blog-wrapper -->

                    <div class="clearfix"></div>
                    
                    <!-- <ul class="pagination">
                        <li class="disabled"><a href="javascript:void(0)">&laquo;</a></li>
                        <li class="active"><a href="javascript:void(0)">1</a></li>
                        <li><a href="javascript:void(0)">2</a></li>
                        <li><a href="javascript:void(0)">3</a></li>
                        <li><a href="javascript:void(0)">...</a></li>
                        <li><a href="javascript:void(0)">&raquo;</a></li>
                    </ul> -->
                </div><!-- end grid -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->




    
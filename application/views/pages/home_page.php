
<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->

<!-- Mirrored from similaricons.com/demos/truvatour/index-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Jul 2017 08:39:08 GMT -->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Metas -->
    <title><?php echo $main_title; ?></title>
    <meta name="keywords" content="Kerala Tour Package , Package Tour Kerala, Kerala Tourism Package,Kerala Tour Place , Destination Tour Kerala , Kerala Tourism Destination">
    <meta name="description" content="Holiday Zone is a travel tour operating company with a difference. We introduce world class travel packages and services for your luxury and comfort. ">
    <meta name="author" content="">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo site_url();?>images/favicon.html" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo site_url();?>images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url();?>images/apple-touch-icon-57x57.html">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url();?>images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url();?>images/apple-touch-icon-76x76.html">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url();?>images/apple-touch-icon-114x114.html">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url();?>images/apple-touch-icon-120x120.html">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url();?>images/apple-touch-icon-144x144.html">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url();?>images/apple-touch-icon-152x152.html">

    <!-- SLIDER STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>rs-plugin/css/settings.css" media="screen" />
    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/bootstrap.css">    
    <!-- ALL CSS -->
    <link rel="stylesheet" href="<?php echo site_url();?>style.css">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/responsive.css">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/custom.html">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/colors.css">

  
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="searchform">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <input type="text" class="form-control" placeholder="What you are looking for?">
                    </form>
                </div>
            </div>
        </div>

        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 hidden-xs">
                        <nav class="topbar-menu">
                            <ul class="list-inline">
                                <li class="hidden-xs"><a href="#"><i class="flaticon-message-closed-envelope"></i> contact@holidayzone.info</a></li>
                                <li><i class="flaticon-telephone"></i>UAE :+971-503859808 , IND:+91-8086044433</li>
                            </ul>
                        </nav><!-- end menu -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12">
                        <nav class="topbar-menu">
                            <ul class="social-links list-inline navbar-right">
                                <li ><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li ><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li ><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li ><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li ><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                           
                        </nav><!-- end menu -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->

        <header class="header">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                             <a class="navbar-brand" href="<?php echo site_url();?>"><img src="<?php echo site_url();?>assets/images/logo.png" alt=""></a>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                
                                <li class="dropdown active">
                                    <a href="<?php echo site_url("/"); ?>" data-target="<?php echo site_url("pages/"); ?>" class="dropdown-toggle" data-toggle="dropdown">Home</a>
                                </li>
                                <li class="dropdown ">
                                    <a href="<?php echo site_url("pages/packages"); ?>" data-target="<?php echo site_url("pages/packages"); ?>" class="dropdown-toggle" data-toggle="dropdown">Tour Packages</a>
                                </li>
                                 <li class="dropdown ">
                                    <a href="<?php echo site_url("pages/destinations"); ?>" data-target="<?php echo site_url("pages/destinations"); ?>" class="dropdown-toggle" data-toggle="dropdown">Destinations</a>
                                </li>
                                <li class="dropdown ">
                                    <a href="<?php echo site_url("pages/about"); ?>" data-target="<?php echo site_url("pages/about"); ?>" class="dropdown-toggle" data-toggle="dropdown">About Holiday Zone</a>
                                </li>

                               

                                <li class="dropdown ">
                                    <a href="<?php echo site_url("pages/reachus"); ?>" data-target="<?php echo site_url("pages/reachus"); ?>" class="dropdown-toggle" data-toggle="dropdown">Reach Us</a>
                                </li>
                                
                                <li><a href="javascript:void(0)" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="flaticon-search"></i></a></li>
                            
                            </ul>
                        </div>
                    </div>
                </nav>
            </div><!-- end container -->
        </header><!-- end header -->
        
        <section class="slider-section">
           <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="<?php echo site_url();?>assets/images/uploads/slider_02.jpg"  data-saveperformance="off"  data-title="Next">
                            <img src="<?php echo site_url();?>assets/images/uploads/slider_01.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="tp-caption slider_layer_02 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="230" 
                                data-speed="1000"
                                data-start="800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                                No Edits No Cuts ,
                            </div>
                            <div class="tp-caption slider_layer_01 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="295" 
                                data-speed="1000"
                                data-start="1200"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                                Its Real As It Gets On Way..
                            </div>
                        </li>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="<?php echo site_url();?>assets/images/uploads/slider_01.jpg"  data-saveperformance="off"  data-title="Next">
                            <img src="<?php echo site_url();?>assets/images/uploads/slider_02.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="tp-caption slider_layer_02 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="230" 
                                data-speed="1000"
                                data-start="800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Have a great weekend with your loved ones
                            </div>
                            <div class="tp-caption slider_layer_01 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="295" 
                                data-speed="1000"
                                data-start="1200"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">We prepared everything for you
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container container-invis">
                <div class="section-big-title text-center">
                    <h3>Most Popular Destinations</h3>
                    <hr>
                    <p>Discover how HolidayZone Tour can you help you find everything you want.</p>
                </div><!-- end title -->

                <div id="owl-tours" class="owl-carousel owl-theme owl-custom custom-tours text-center">
                    <?php 
                        foreach ($destinations_details  as $destination) { 
                        $redirect_url=base_url().'pages/DestinationDetails/'.$destination['DestinationId'];
                        ?>
                        <div class="owl-item">
                            <div class="car-wrapper clearfix">
                                <div class="post-media entry">
                                    <img src="http://adm.holidayzone.info/assets/DestinationImages/<?php echo $destination['DestinationImage']; ?>" alt="" class="img-responsive">
                                    <div class="magnifier">
                                    </div><!-- end magnifier -->
                                    <div class="invis-title">
                                        <h3><?php echo $destination['DestinationName']; ?></h3>
                                    </div>
                                    <ul class="list-inline">
                                        <li class="car-km">
                                            <p><a href="<?php echo  base_url().'pages/DestinationDetails/'.$destination['DestinationId']; ?>"><i class="fa fa-search"></i> View Details </a></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    <?php 
                     }
                    ?>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section lb">
            <div class="container container-invis">
                <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <div class="section-big-title small-title-area text-left">
                            <i class="flaticon-details alignleft"></i>
                            <h3>Get your dreams!</h3>
                            <p>With HolidayZone Tourism Get Your Dream Tour Today!</p>
                        </div><!-- end title -->

                        <div class="about-widget">
                            <img src="<?php echo site_url();?>assets/images/uploads/about_01.jpg" alt="slider q" class="img-responsive alignleft">
                            <p> In sagittis orci id urna varius, vitae dignissim est rutrum. Quisque scelerisque ultrices eros, id aliquam magna placerat id. Sed eget vulputate urna, ut congue lacus. Duis risus magna, vestibulum id tincidunt a, aliquet sit amet felis.  </p>
                            
                            <hr>

                            <div class="cdetails">
                                <p><i class="flaticon-clock-circular-outline"></i> 7 Day 6 Night - Son aliquet sit amet felis</p>
                                <p><i class="flaticon-air-transport"></i> Ait Travel Free - Maaliquet sit amet felis</p>
                                <p><i class="flaticon-radio"></i> Night Club - Inaliquet sit amet felis</p>
                            </div><!-- end cdetails -->
                        </div><!-- end about-widget -->
                    </div>

                    <div class="col-md-5 col-sm-12">
                        <div class="section-big-title small-title-area text-left">
                            <i class="flaticon-smiling-emoticon-square-face alignleft"></i>
                            <h3>Happy Clients</h3>
                            <p>What Others Say About HolidayZone</p>
                        </div><!-- end title -->

                        <div id="owl-testimonials" class="owl-carousel owl-theme owl-custom owl-invis botleft">
                            <div class="owl-item">
                                <div class="client-box">
                                <img src="<?php echo site_url();?>assets/images/uploads/people_02.jpg" alt="" class="img-responsive img-circle">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <h4>Fantastic Job! Thanks make my dream!</h4>
                                <p>In sagittis orci id urna varius, vitae dignissim est rutrum. Quisque scelerisque ultrices eros, id aliquam magna placerat id. Sed eget vulputate urna, ut congue lacus.  </p>
                                </div>
                            </div>

                            <div class="owl-item">
                                <div class="client-box">
                                <img src="<?php echo site_url();?>assets/images/uploads/people_03.jpg" alt="" class="img-responsive img-circle">
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <h4>I'll come back again!</h4>
                                <p>In sagittis orci id urna varius, vitae dignissim est rutrum. Quisque scelerisque ultrices eros, id aliquam magna placerat id. Sed eget vulputate urna, ut congue lacus.  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section"></section>
  <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Latest Packages</h4>
                                <hr>
                            </div>

                            <ul class="blog-posts">
                               <?php 
                                $i=0;
                                foreach ($packages_details as $packages) { 
                                    $redirect_url=base_url().'pages/PackageDeatils/'. $packages['PackageId'];
                                    $package_description=$packages["PackageDescription"];
                                    $package_description = character_limiter($package_description,80);
                                      ?>
                                    <li>
                                    <a href="<?php echo  $redirect_url;  ?>"><img src="http://adm.holidayzone.info/assets/PackageImages/<?php echo $packages['PackageImage']; ?>" alt="<?php echo $packages['PackageTitle']; ?>" class="img-responsive alignleft"></a>
                                    <h3><a href="<?php echo  $redirect_url;  ?>"><?php echo $packages['PackageTitle']; ?></a></h3>
                                    <p><?php echo $package_description; ?></p>
                                    <small></small>
                                    </li>
                                <?php
                                $i=$i+1;
                                if($i==2)
                                    break;
                                }
                                ?>
                            </ul><!-- end blog-posts -->
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Quick Links</h4>
                                <hr>
                            </div>

                            <!-- <ul class="twitter-posts"> -->
                                   <ul class="twitter-posts">
                                    <li><a href="<?php echo site_url("/"); ?>">Home</a></li>
                                    <li><a href="<?php echo site_url("pages/about"); ?>">About Us</a></li>
                                    <li><a href="<?php echo site_url("pages/reachus"); ?>">Contact Us</a></li>
                                    <li><a href="<?php echo site_url("pages/packages"); ?>">Packages</a></li>
                                    <li><a href="<?php echo site_url("pages/destinations"); ?>">Destination</a></li>
                                </ul><!-- end list -->
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>About us</h4>
                                <hr>
                            </div>

                            <p>Holiday Zone is a travel tour operating company with a difference. We introduce world class travel packages and services for your luxury and comfort. Our tour package itineraries are hand-picked according to client’s choice and are travelled by our travel representatives. We customize your travel plan as per your interest and preferences.</p>

                            <ul class="social-links list-inline">
                                <li class="icon facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="icon twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="icon google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="icon instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="icon pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li class="icon youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li class="icon rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul><!-- end social -->
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->


                <div class="row"></div><!-- end row -->
            </div><!-- end container -->
        </footer>

        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; HolydayZone INC. All rights reserved 2017.</p>
                    </div><!-- end col -->

                    <div class="col-md-6 text-right hidden-xs">
                        <ul class="list-inline">
                            <li><a href="<?php echo site_url("/"); ?>">Home</a></li>
                            <li><a href="<?php echo site_url("pages/terms"); ?>">Terms of Usage</a></li>
                            <li><a href="<?php echo site_url("pages/privacy"); ?>">Privacy Policy</a></li>
                            <li class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></li>
                        </ul>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </div>
    <!-- end wrapper -->

 <!-- Java Script
================================================== --> 
 <script src="<?php echo site_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo site_url();?>assets/js/bootstrap.js"></script>
    <script src="<?php echo site_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo site_url();?>assets/js/parallax.js"></script>
    <script src="<?php echo site_url();?>assets/js/animate.js"></script>
    <script src="<?php echo site_url();?>assets/js/custom.js"></script>
    <script src="<?php echo site_url();?>assets/js/fitvid.js"></script>
    <!-- FOR SINGLE LISTING PAGES -->
    <script src="<?php echo site_url();?>assets/js/bootstrap-select.js"></script>
    <script src="<?php echo site_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo site_url();?>assets/js/price-ranger.js"></script>
    <!-- SLIDER REV -->
    <script src="<?php echo site_url();?>rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo site_url();?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script>
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:600,
            hideThumbs:200,     
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,  
            navigationType:"none",
            navigationArrows:"solo",
            navigationStyle:"preview2",  
            touchenabled:"on",
            onHoverStop:"on",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,          
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
            parallaxDisableOnMobile:"off",           
            keyboardNavigation:"off",   
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,  
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",  
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",  
            autoHeight:"off",           
            forceFullWidth:"off",                         
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,            
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            fullScreenOffsetContainer: ""  
            }); 
        });
    </script>



    </body>
</html>
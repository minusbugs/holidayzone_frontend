<section class="section lb page-title little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix">
                    <div class="pull-left">
                        <h3>About Holiday Zone </h3>
                        <p>Travel & Tour operating company .</p>
                    </div>

                    <div class="pull-right hidden-xs">
                        <ul class="breadcrumb">
                            <li><a href="javascript:void(0)">Home</a></li>
                            <li class="active">About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-meta">
                    <h3>Overview</h3>
                    <p>Holiday Zone is a travel tour operating company with a difference. We introduce world class travel packages and services for your luxury and comfort. Our tour package itineraries are hand-picked according to client’s choice and are travelled by our travel representatives. We customize your travel plan as per your interest and preferences.</p>
                    <p> Now we have introduced in India with more packages and offers. This gives an opportunity for Indian clients to enjoy European travel journey more localized. We also provide our travel services in India.</p>
                    <p>Travelling abroad is little complex due to foreign languages, weather conditions and having the right food. A good travel agent can provide the best packages at the same time taking care of the clients.</p>
                    <p>Our packages provide the feel in culture and its differences of each country. Its our pleasure to serve all categories of travel groups as family tours, honeymoon trips, cruises, ladies only, corporate trips, one day trips, adventure and wildlife, wine tasting tours and Ayurveda and spa wellness tourism.</p>
                    <p>In every Group trip our English speaking representative will pick you up from the airport and travel along with the group according to the itinerary. With respect to the Clients Interest, group trips can be customized with extra sightseeing or entertainment to the travel itinerary.</p>
                    <p>Allow us to plan your travel ideas, we can be contacted through phone, email or drop us a message so that we can get touch with you at the earliest.</p>

                    <h3>Why we choose?</h3>

                    <div class="cdetails">
                        <p><i class="flaticon-smiling-emoticon-square-face"></i> Quality of Service: Highest in travel industry with hand picked itineraries.</p>
                        <p><i class="flaticon-smiling-emoticon-square-face"></i> 24/7 Personal Care: Dedicated tour managers, it can be emergency, booking changes or request issues.</p>
                        <p><i class="flaticon-smiling-emoticon-square-face"></i> Flexibility & Customization: Plan your dream with no rigidity, book desired hotels and tours.</p>
                        <p><i class="flaticon-smiling-emoticon-square-face"></i>Transparency: Instant confirmation in 20 mins with no hidden charges..</p>
                        <p><i class="flaticon-smiling-emoticon-square-face"></i> Wide range of selection: Choose any hotels, airlines, cruises, entertainments and destinations..</p>
                        <p><i class="flaticon-smiling-emoticon-square-face"></i> Free higher upgrades: If you are lucky we can upgrade your travel plans to luxury with available slots..</p>
                    
                    </div>
                </div>
            </div>
              <div class="clearfix"></div>
        </div>
    </div>
</section>
  
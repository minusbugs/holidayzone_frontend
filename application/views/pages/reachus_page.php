 <section class="section lb page-title little-pad">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-big-title clearfix">
                            <div class="pull-left">
                                <h3>Contact Us</h3>
                                <p>If you have any pre-sale questions please send us an email.</p>
                            </div>

                            <div class="pull-right hidden-xs">
                                <ul class="breadcrumb">
                                    <li><a href="javascript:void(0)">Home</a></li>
                                    <li class="active">Contact</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>

<section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <div class="blog-wrapper">
                                <div class="blog-item">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <form class="contact-form search-top">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Full Name">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                                                    </div>
                                                </div>
                                               

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="subject" placeholder="Sıbject">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <textarea placeholder="Your message" class="form-control"></textarea>
                                                    <button class="btn btn-primary" type="submit">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="default">
                                                 <h2>Or contact us directly</h2>
                                                 <ul class="list-inline">
                                                    <li><i class="flaticon-message-closed-envelope"></i> 31/1205,Vytilla Kochi </li>
                                                   <li class="hidden-xs"><a href="#"><i class="flaticon-message-closed-envelope"></i> contact@holidayzone.info</a></li>
                                                    <li><i class="flaticon-telephone"></i> +97-1503859808 , +91-8086044433 ,+91 8606067974 </li>
                                                   
                                                </ul>
                                               
                                               
                                            </div>
                                        </div><!-- end col -->
                                    </div>
                                </div><!-- end blog-item -->
                            </div>
                        </div><!-- end grid -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
</section><!-- end section -->
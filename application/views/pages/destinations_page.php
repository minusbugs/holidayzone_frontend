<section class="section lb page-title little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix">
                    <div class="pull-left">
                        <h3>All Destination List</h3>
                       
                    </div>

                    <div class="pull-right hidden-xs">
                        <ul class="breadcrumb">
                            <li><a href="javascript:void(0)">Home</a></li>
                            <li><a href="javascript:void(0)">Destination</a></li>
                            <li class="active">destination All</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Bind Package Details   -->

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div class="blog-wrapper blog-masonry row">
                        <?php 
                             $i=0;
                                foreach ($destinations_details  as $destination) {
                                    $redirect_url=base_url().'pages/DestinationDetails/'.$destination['DestinationId'];
                                   
                                    $div_priority="";
                                    if($i==0){
                                        $div_priority="first";
                                    }elseif($i==2){
                                        $div_priority="last";
                                       
                                    }
                        ?>

                                <div class="col-md-4 col-sm-6 <?php echo $div_priority; ?> ">
                                    <div class="list-item">
                                        <a href="<?php echo  $redirect_url;  ?>" title="">
                                        <div class="entry">
                                            <img src="http://adm.holidayzone.info/assets/DestinationImages/<?php echo $destination['DestinationImage']; ?>" alt="<?php echo $destination['DestinationName']; ?>" class="img-responsive">
                                            <div class="magnifier">
                                                <div class="magni-desc">
                                                </div>
                                            </div><!-- end magnifier -->
                                        </div><!-- end entry -->
                                        </a>
                                        <div class="list-desc clearfix">
                                            <h4><a href="<?php echo  $redirect_url;  ?>" title=""><?php echo $destination['DestinationName']; ?></a></h4>
                                            <a href="<?php echo  $redirect_url;  ?>" class="readmore">View Packages <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                        <?php
                        $i=$i+1;
                        if($i==3){
                            $i=0;
                        }
                                    }
                        ?>

                    
                    </div><!-- end blog-wrapper -->

                    <div class="clearfix"></div>
                    
                    <!-- <ul class="pagination">
                        <li class="disabled"><a href="javascript:void(0)">&laquo;</a></li>
                        <li class="active"><a href="javascript:void(0)">1</a></li>
                        <li><a href="javascript:void(0)">2</a></li>
                        <li><a href="javascript:void(0)">3</a></li>
                        <li><a href="javascript:void(0)">...</a></li>
                        <li><a href="javascript:void(0)">&raquo;</a></li>
                    </ul> -->
                </div><!-- end grid -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->




    
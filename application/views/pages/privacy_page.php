<section class="section lb page-title little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix">
                    <div class="pull-left">
                        <h3>Privacy Policy </h3>
                        
                    </div>

                    <div class="pull-right hidden-xs">
                        <ul class="breadcrumb">
                            <li><a href="javascript:void(0)">Home</a></li>
                            <li class="active">Privacy Policy</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-meta">
                    <h3>Privacy Policy</h3>
                    <ul>
                        <li>Your accommodation is selected as per your budget.</li>
                        <li>We do not have any hidden charges except your lunch and dinner.</li>
                        <li> Any extra bed/ breakfast which are not mentioned have to be paid directly by guest as per hotel policy.</li>
                        <li>The above rates are valid for the mentioned period only.</li>
                        <li> In Munnar and Thekaddey,Ootty and Kodaikinal rooms will not be A/c since these are cold place.</li>
                        <li>In case of any unexpected bandh or strike, we will make alternative arrangements, please bare with us.</li>
                        <li>Balance payment will not be accepted by Cheque or DD</li>
                    </ul>
                </div>
            </div>
              <div class="clearfix"></div>
        </div>
    </div>
</section>
  
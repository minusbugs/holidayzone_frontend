<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->

<!-- Mirrored from similaricons.com/demos/truvatour/index-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Jul 2017 08:39:08 GMT -->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Metas -->
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url();
?>images/favicon.html" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo base_url();
?>images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();
?>images/apple-touch-icon-57x57.html">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();
?>images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();
?>images/apple-touch-icon-76x76.html">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();
?>images/apple-touch-icon-114x114.html">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();
?>images/apple-touch-icon-120x120.html">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();
?>images/apple-touch-icon-144x144.html">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();
?>images/apple-touch-icon-152x152.html">

    <!-- SLIDER STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();
?>rs-plugin/css/settings.css" media="screen" />
    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" href="<?php echo base_url();
?>assets/css/bootstrap.css">    
    <!-- ALL CSS -->
    <link rel="stylesheet" href="<?php echo base_url();
?>style.css">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="<?php echo base_url();
?>assets/css/responsive.css">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="<?php echo base_url();
?>assets/css/custom.html">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="<?php echo base_url();
?>assets/css/colors.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="searchform">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <input type="text" class="form-control" placeholder="What you are looking for?">
                    </form>
                </div>
            </div>
        </div>

        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 hidden-xs">
                        <nav class="topbar-menu">
                            <ul class="list-inline">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Partners</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">E-Catalog</a></li>
                                <li><a href="#">Subscribe</a></li>
                            </ul><!-- end list -->
                        </nav><!-- end menu -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12">
                        <nav class="topbar-menu">
                            <ul class="list-inline text-right navbar-right">
                               
                                <li class="hidden-xs"><a href="#"><i class="flaticon-message-closed-envelope"></i> Contact</a></li>
   <!--                              <li class="hidden-xs"><i class="flaticon-clock-circular-outline"></i> 08:00 - 17:00</li> -->
                                <li><i class="flaticon-telephone"></i> +97 1503859808 , +91 8086044433</li>
                            </ul><!-- end list -->
                        </nav><!-- end menu -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->

        <header class="header">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                             <a class="navbar-brand" href="/"><img src="assets/images/logo.png" alt=""></a>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                
                                <li class="dropdown active">
                                    <a href="" data-target="" class="dropdown-toggle" data-toggle="dropdown">Home</a>
                                </li>
                                <li class="dropdown ">
                                    <a href="" data-target="" class="dropdown-toggle" data-toggle="dropdown">Tour Packages</a>
                                </li>
                                 <li class="dropdown ">
                                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Destinations</a>
                                </li>
                                <li class="dropdown ">
                                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">About Holiday Zone</a>
                                </li>

                               

                                <li class="dropdown ">
                                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Reach Us</a>
                                </li>
                                
                                <li><a href="javascript:void(0)" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="flaticon-search"></i></a></li>
                            
                            </ul>
                        </div>
                    </div>
                </nav>
            </div><!-- end container -->
        </header><!-- end header -->